
def pixelGenerateFromString(str):
	if not str.startswith( '(' ):
		str = str.upper()
		if str == 'WHITE':
			return Pixel(255,255,255)
		elif str == 'BLACK':
			return Pixel(0,0,0)
		elif str == 'RED':
			return Pixel(255,0,0)
		elif str == 'GREEN':
			return Pixel(0,255,0)
		elif str == 'BLUE':
			return Pixel(0,0,255)

		raise ValueError("Invalid pixel value " + str)
	if not str.endswith( ')' ):
		raise ValueError("Invalid pixel value " + str)
	str = str[1:-1]
	rgbarray = str.split(",")
	if (len(rgbarray)!=3):
		raise ValueError("Invalid pixel value " + str)

	return Pixel(
		int(rgbarray[0]),
		int(rgbarray[1]),
		int(rgbarray[2])
	)

class Pixel():
	r_val = 0
	g_val = 0
	b_val = 0
	def __init__(self,r,g,b):
		if (r<0) or (r>255):
			raise ValueError("Bad pixel r value " + r)
		if (g<0) or (g>255):
			raise ValueError("Bad pixel g value " + g)
		if (b<0) or (b>255):
			raise ValueError("Bad pixel b value " + b)
		self.r_val = r
		self.g_val = g
		self.b_val = b

	def copy(self):
		#returns an independaat copy
		return Pixel(self.r_val, self.g_val, self.b_val)

class SequenceFrame():
	pixelValues = []
	transistionTime = 1 #time to transistion into this frame
	holdTime = 0 #time to hold this frame, 0 to change away as soon as transisition completes
	nextFrame = -1 #next frame to display, -1 to never change away from this frame

	def __init__(self, jsonStructure, frameNum):
		self.pixelValues = [[0 for x in range(8)] for y in range(8)] 
		#Setup values based on json structure

		jsonPixelValues = jsonStructure.get('pixelValues')
		if (len(jsonPixelValues)!=(8*8)):
			if (len(jsonPixelValues)!=(1)):
				raise ValueError("Must have " + str(8*8) + " pixels in a frame - frame, (or 1 pixel for solid frame) " + str(frameNum) + " has " + str(len(jsonPixelValues)))

		if (len(jsonPixelValues)==(1)):
			targetDisplayA = [[0 for x in range(8)] for y in range(8)] 
			for x in range(0,8):
				for y in range(0,8):
					self.pixelValues[x][y] = pixelGenerateFromString(jsonPixelValues[0])
		else:
			col = 0
			row = 0
			for curPixel in jsonPixelValues:
				self.pixelValues[col][row] = pixelGenerateFromString(curPixel)
				col = col + 1
				if col>7:
					col=0
					row = row + 1

		self.transistionTime = jsonStructure.get('transistionTime')
		self.holdTime = jsonStructure.get('holdTime')
		self.nextFrame = jsonStructure.get('nextFrame')




class Sequence():
	frames = [] #pixel values for each frame, each frame has a number 0, 1, ...
	startFrame = 0 #first frame to display
	motionFrame = 0 #frame to jump to when motion is detected
	motionTimeout = 60 * 5 #time to start count down from when motoin is deteceted
	motionTimeoutFrame = 0 #frame to jump to when motion times out - 0 for no jump
	motionRedetectJump = False #if True will jump back to motion frame each time motion is redetecetd
	name = "Unknown" #name of the sequence. used in log files

	def __init__(self, jsonStructure):
		jsonFrames = jsonStructure.get('frames')


		if (len(jsonFrames)==0):
			raise ValueError("Must have at least one frame")

		self.frames = [0 for x in range(len(jsonFrames))]
		frameNum = 0
		for a in jsonFrames:
			self.frames[frameNum] = SequenceFrame(a, frameNum)
			if (self.frames[frameNum].nextFrame != -1):
				EnsureValidFrame(len(self.frames), self.frames[frameNum].nextFrame, "Frame " + str(frameNum) + " next Frame is invalid")
				if (self.frames[frameNum].nextFrame == frameNum):
					raise ValueError("Frame " + str(frameNum) + " next Frame points to itself. (Use -1 for no next frame)")
			frameNum = frameNum + 1

		self.startFrame = jsonStructure.get('startFrame')
		self.motionFrame = jsonStructure.get('motionFrame')
		self.motionTimeout = jsonStructure.get('motionTimeout')
		self.motionTimeoutFrame = jsonStructure.get('motionTimeoutFrame')
		self.motionRedetectJump = jsonStructure.get('motionRedetectJump')
		self.name = jsonStructure.get('name')
		if self.name is None:
			self.name = "Unknown"
		if self.name == "":
			self.name = "Unknown"

		EnsureValidFrame(len(self.frames), self.startFrame, "Start frame is invalid")
		EnsureValidFrame(len(self.frames), self.motionFrame, "Motion Frame frame is invalid")
		EnsureValidFrame(len(self.frames), self.motionTimeoutFrame, "Motion Timeout Frame frame is invalid")

		if (self.motionTimeout<0):
			raise ValueError('Must have positive motionTimout value, (0 for no timeout)')

def EnsureValidFrame(numFrames, frameNum, errMsg):
	if (frameNum>=numFrames):
		raise ValueError(errMsg)
	if (frameNum<0):
		raise ValueError(errMsg)

class RunningSequence():
	sequence = None
	curFrame = -1 #-1 means the running sequence has not been inited
	frameChangeTime = None
	recievedMotionDetectSignal = False #True if motion has been detected but not processed in run loop
	motionTimeoutTime = None
	motionDetectedMode = False
	logFn = None

	def __init__(self, seq, logFn):
		self.sequence = seq
		self.curFrame = -1
		self.logFn = logFn

	def notifyMotion(self):
		self.recievedMotionDetectSignal = True

	def setFrame(self, frameNum, displayPattern, curTime):
		self.curFrame = frameNum
		displayPattern.setTargetDisplay(self.sequence.frames[self.curFrame].pixelValues,self.sequence.frames[self.curFrame].transistionTime)
		self.frameChangeTime = curTime + self.sequence.frames[self.curFrame].transistionTime + self.sequence.frames[self.curFrame].holdTime

	def log(self, message):
		if self.sequence is None:
			self.logFn("**None**," + message)
		else:
			self.logFn(self.sequence.name + "," + message)

	def runLoop(self, curTime, displayPattern):
		if self.curFrame==-1:
			# print("Setting start frame to " + str(self.sequence.startFrame))
			self.setFrame(self.sequence.startFrame, displayPattern, curTime)

		if self.frameChangeTime is not None:
			if self.frameChangeTime < curTime:
				if self.sequence.frames[self.curFrame].nextFrame != -1:
					self.setFrame(self.sequence.frames[self.curFrame].nextFrame, displayPattern, curTime)

		if self.recievedMotionDetectSignal:
			self.recievedMotionDetectSignal = False
			if self.sequence.motionTimeout > 0:
				self.motionTimeoutTime = curTime + self.sequence.motionTimeout
			# print("Setting motion timout to " + str(self.motionTimeoutTime))
			if self.motionDetectedMode:
				# print("Running Sequence motion Detect - in Thread runLoop - already in motion det mode")
				self.log("motionDetectContinue")
				if self.sequence.motionRedetectJump:
					self.setFrame(self.sequence.motionFrame, displayPattern, curTime)
			else:
				self.motionDetectedMode = True
				self.log("motionDetectStart")
				# print("Motion detect mode started")
				self.setFrame(self.sequence.motionFrame, displayPattern, curTime)

		if self.motionTimeoutTime is not None:
			if self.motionTimeoutTime < curTime:
				self.log("motionDetectEnd")
				self.motionTimeoutTime = None
				self.motionDetectedMode = False
				# print("Motion detect mode ended")
				self.setFrame(self.sequence.motionTimeoutFrame, displayPattern, curTime)


