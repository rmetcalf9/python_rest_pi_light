#include "DisplayPattern.h"

DisplayPattern::DisplayPattern(unsigned long currentTime) {
  for (int c=0;c<NUM_LEDS;c++) {
    curDisplay[c] = new Pixel(c,0,0,0);
    frameStart[c] = new Pixel(c,0,0,0);
    frameTarget[c] = new Pixel(c,0,0,0);
  }
  setTargetDisplaySolidColour(currentTime, 0,0,0,10000); //go to white in 10 seconds
  return;
}
DisplayPattern::~DisplayPattern() {
  for (int c=0;c<NUM_LEDS;c++) {
    SAFE_DELETE(curDisplay[c]);
    SAFE_DELETE(frameStart[c]);
    SAFE_DELETE(frameTarget[c]);
  };
  return;
}

// Sets up values for the frame
void DisplayPattern::runLoop(unsigned long currentTime, Adafruit_NeoPixel* leds) {
  for (int c=0;c<NUM_LEDS;c++) {
    curDisplay[c]->setRed(getNewCurValue(
      frameStart[c]->getRed(), 
      frameStartTransistionTime, 
      currentTime, 
      frameTarget[c]->getRed(), 
      frameEndTransistionTime
    ));
    curDisplay[c]->setGreen(getNewCurValue(
      frameStart[c]->getGreen(), 
      frameStartTransistionTime, 
      currentTime, 
      frameTarget[c]->getGreen(), 
      frameEndTransistionTime
    ));
    curDisplay[c]->setBlue(getNewCurValue(
      frameStart[c]->getBlue(), 
      frameStartTransistionTime, 
      currentTime, 
      frameTarget[c]->getBlue(), 
      frameEndTransistionTime
    ));
  };

  for(uint16_t i=0; i<NUM_LEDS; i++) {
    curDisplay[i]->display(leds);
  };

  return;
}

void DisplayPattern::setTargetDisplaySolidColour(
  unsigned long currentTime, 
  unsigned int red, 
  unsigned int green, 
  unsigned int blue, 
  unsigned long transisitionTime
) {
  Pixel* newDisplay[NUM_LEDS];
  for (int c=0;c<NUM_LEDS;c++) {
    newDisplay[c] = new Pixel(c,red,green,blue);
  }
  setTargetDisplay(currentTime, newDisplay, transisitionTime);
  for (int c=0;c<NUM_LEDS;c++) {
    SAFE_DELETE(newDisplay[c]);
  }
}
void DisplayPattern::setTargetDisplay(unsigned long currentTime, Pixel* newTargetDisplay[], unsigned long transisitionTime) {
  for (int c=0;c<NUM_LEDS;c++) {
    frameStart[c]->setColourMatch(curDisplay[c]);
    frameTarget[c]->setColourMatch(newTargetDisplay[c]); 
  }
  frameStartTransistionTime = currentTime;
  frameEndTransistionTime = currentTime + transisitionTime;

  return;
}
unsigned int DisplayPattern::getNewCurValue(unsigned int startVal, unsigned long startTime, unsigned long curTime, unsigned int endVal, unsigned long endTime) {
  if (curTime > endTime) return endVal;
  double ratio = (double) (endVal - startVal)/(double) ((endTime-startTime));
  unsigned int newVal = startVal + int(round(ratio * (curTime - startTime)));
  if (startVal<endVal) {
    if (newVal>endVal) {
      newVal = endVal;
    }
  } else {
    if (newVal<endVal) {
      newVal = endVal;
    }
  }
  if (newVal<0) newVal = 0;
  if (newVal>255) newVal = 255;
  return newVal;
}
