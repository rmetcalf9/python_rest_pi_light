import threading
import datetime, time
from flask import Blueprint, abort, g, jsonify, request
from time import gmtime, strftime
from gpiozero import MotionSensor

sensor_api_blueprint = Blueprint('sensor_api_blueprint',__name__);


@sensor_api_blueprint.route('/api/sensor/status')
def sensor_status():
	#if not request.json:
	#	abort(415)
	#json = request.json

	return sensorReader.getStateJSON()



class SensorReaderC(threading.Thread):
	pir = MotionSensor(14)
	mot_coun = 0
	last_round_det = False
	lastMotionDetectTime = datetime.datetime.now()
	running = True
	unicornController = None

	def __init__(self):
		threading.Thread.__init__(self)

	def setUnicornController(self, unicornControllerP):
		self.unicornController = unicornControllerP

	def run(self):
		self.running = True
		while self.running:
			while self.pir.motion_detected:
				if not self.last_round_det:
					self.last_round_det = True
					self.mot_coun = self.mot_coun + 1
					self.lastMotionDetectTime=datetime.datetime.now()
					self.unicornController.notifyMotion()
					#print(strftime("%Y-%m-%d %H:%M:%S", gmtime()) + " " + str(self.mot_coun) + " - Motion detected!")
				#while waiting for motion to stop we only check every second
				time.sleep(1)

			# no motion (detected is false)
			if self.last_round_det:
				# in the last round detected was true for it has just stopped, register the end of the motion
				self.last_round_det = False
				#print(strftime("%Y-%m-%d %H:%M:%S", gmtime()) + " Motion end")
			#while waiting for motion to stop we only check every two tenth of a second
			time.sleep(0.2)

	def getStateJSON(self):
		return jsonify(
			currentMotion=self.last_round_det,
			lastMotionDetectTime=self.lastMotionDetectTime.isoformat(),
			motionCount=self.mot_coun
		)

	def stopRunning(self):
		self.running = False

sensorReader = SensorReaderC()
