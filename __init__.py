from auth import masterAuthObj

__all__ = ["main"]

#register all custom roles. This allows role names to be added to settings file
masterAuthObj.registerInternalRoleName('CUSTOM','custom')


