#include "neoPixel.h"
#include "consts.h"
#include <ArduinoJson.h>

#define NEOPIXEL_PIN 14 // NeoPixels are connected to this pin



RJM_NeoPixel::RJM_NeoPixel() {
  leds = new Adafruit_NeoPixel(NUM_LEDS, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);
  displayPattern = new DisplayPattern(millis());
  return;
}
RJM_NeoPixel::~RJM_NeoPixel() {
  delete leds;
  delete displayPattern;
  return;
}

void RJM_NeoPixel::Setup() {
  leds->begin();
  leds->setBrightness(128); //0=0% 255=100%
  leds->clear(); // All NeoPixels off ASAP
  leds->show();
  return;
}

void RJM_NeoPixel::Render(unsigned long currentTime) {
  displayPattern->runLoop(currentTime, leds);
  leds->show();
}

Pixel* RJM_NeoPixel::GeneratePixel(unsigned int ppixel_index, const char * buf) {
  if (strcmp(buf,"WHITE")==0) return new Pixel(0,255,255,255);
  if (strcmp(buf,"BLACK")==0) return new Pixel(0,0,0,0);
  if (strcmp(buf,"RED")==0) return new Pixel(0,255,0,0);
  if (strcmp(buf,"GREEN")==0) return new Pixel(0,0,255,0);
  if (strcmp(buf,"BLUE")==0) return new Pixel(0,0,0,255);

  unsigned int col[3];
  col[0] = 0;
  col[1] = 0;
  col[2] = 0;
  char readBuf[10] = "";
  char tt[2] = "";
  tt[0]='\0';
  tt[1]='\0';

  unsigned int curCol = 0;
  for (int c=0;c<strlen(buf);c++) {
    tt[0] = buf[c];
    if (buf[c]==',') {
      if (curCol>2) return NULL;
      sscanf (readBuf,"%d",&col[curCol]);
      curCol++;
      readBuf[0] = '\0';
    } else if (buf[c]==' ') {
    } else if (buf[c]=='(') {
    } else if (buf[c]==')') {
    } else {
      if (strlen(readBuf)>9) return NULL;
      strcat(readBuf,tt);
    }
  }
  sscanf (readBuf,"%d",&col[curCol]);
  
  return new Pixel(ppixel_index, col[0], col[1], col[2]);
};


void RJM_NeoPixel::SetFrame(unsigned long currentTime, String incommingJSON, unsigned int *returnVal, char* resultJSON) {
  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(incommingJSON);
  if (!root.success()) {
    strcpy(resultJSON, "{\"Result\":\"ERROR\", \"Message\": \"Error parsing JSON\"}");
    *returnVal = 500;
    return;    
  }

/*
 * Example JSON
 * "pixelValues": [
        "RED"
      ],
 * "transistionTime": 0
 */
  if (!root.containsKey("transistionTime")) {
    strcpy(resultJSON, "{\"Result\":\"ERROR\", \"Message\": \"Error missing transistionTime in input\"}");
    *returnVal = 500;
    return;    
  }
  long transisitionTime = root["transistionTime"];

  JsonArray& pixelValueArr = root["pixelValues"];
  if (!pixelValueArr.success()) {
    strcpy(resultJSON, "{\"Result\":\"ERROR\", \"Message\": \"Error missing pixelValues in input\"}");
    *returnVal = 500;
    return; 
  }
  int numjsonvals = pixelValueArr.size();

  Pixel* newDisplay[NUM_LEDS];
  int jsonNum = 0;
  for (int c=0;c<NUM_LEDS;c++) {
    jsonNum = (c % numjsonvals);
    newDisplay[c] = GeneratePixel(c,pixelValueArr.get<const char*>(jsonNum));
    if (NULL == newDisplay[c]) {
      strcpy(resultJSON, "{\"Result\":\"ERROR\", \"Message\": \"Error bad pixel value\"}");
      *returnVal = 500;
      return;      
    }
  }

  if (root.containsKey("brightness")) {
    long brightness = root["brightness"];
    leds->setBrightness(brightness); //0=0% 255=100%
  }

  displayPattern->setTargetDisplay(currentTime, newDisplay, transisitionTime);
  for (int c=0;c<NUM_LEDS;c++) {
    SAFE_DELETE(newDisplay[c]);
  }

  
  strcpy(resultJSON, "{\"Result\":\"OK\", \"Message\": \"Success\"}");
  *returnVal = 200;
  return;
}
