import threading
import time, math, os.path
import unicornhat as unicorn
from random import randint
from time import strftime, gmtime

from flask import Blueprint, jsonify, request, abort, make_response, json
from service_code.lightSequence import Pixel, Sequence, RunningSequence
#from time import gmtime, strftime
#from gpiozero import MotionSensor


#from time import gmtime, strftime

unicorn_api_blueprint = Blueprint('unicorn_api_blueprint',__name__);
displayPattern = []


sequenceSaveFileName = './py_packages/service_code/lastRecievedSequence.json'

xxx = 0
targetDisplayA = [[0 for x in range(8)] for y in range(8)] 
for x in range(0,8):
	for y in range(0,8):
		targetDisplayA[x][y] = Pixel(0,0,255)
targetDisplayB = [[0 for x in range(8)] for y in range(8)] 
for x in range(0,8):
	for y in range(0,8):
		targetDisplayB[x][y] = Pixel(255,0,0)

@unicorn_api_blueprint.route('/api/unicorn/debug/<time>')
def sensor_status(time):
	global xxx, displayPattern
	#if not request.json:
	#	abort(415)
	#json = request.json


	set_to = 'A'
	if xxx == 0:
		xxx = 1
		set_to = 'A'
		displayPattern.setTargetDisplay(targetDisplayA,float(time))
	elif xxx == 1:
		xxx = 0
		set_to = 'B'
		displayPattern.setTargetDisplay(targetDisplayB,float(time))
	

	return jsonify(result='ok',setTo=set_to)

@unicorn_api_blueprint.route('/api/unicorn/setsequence', methods=['POST'])
def set_sequence():
	global sequenceSaveFileName
	if not request.json:
		abort(415)
	jsonObj = request.json

	try:
		displayPattern.setSequence(Sequence(jsonObj))
	except ValueError as e:
		return make_response(jsonify(
			response="error",
			message=str(e)
		),400)

	# only save if it was correct
	f = open(sequenceSaveFileName, 'w')
	f.write(json.dumps(jsonObj))
	f.close

	return jsonify(result='ok')

@unicorn_api_blueprint.route('/api/unicorn/debug')
def sensor_status2():
	return sensor_status(100)

unicorn.set_layout(unicorn.HAT)
unicorn.rotation(0)
unicorn.brightness(0.5)
width,height=unicorn.get_shape()

points = []


class LightPoint:

    def __init__(self):

        self.direction = randint(1, 4)
        if self.direction == 1:
            self.x = randint(0, width - 1)
            self.y = 0
        elif self.direction == 2:
            self.x = 0
            self.y = randint(0, height - 1)
        elif self.direction == 3:
            self.x = randint(0, width - 1)
            self.y = height - 1
        else:
            self.x = width - 1
            self.y = randint(0, height - 1)

        self.colour = []
        for i in range(0, 3):
            self.colour.append(randint(100, 255))


def update_positions():

    for point in points:
        if point.direction == 1:
            point.y += 1
            if point.y > height - 1:
                points.remove(point)
        elif point.direction == 2:
            point.x += 1
            if point.x > width - 1:
                points.remove(point)
        elif point.direction == 3:
            point.y -= 1
            if point.y < 0:
                points.remove(point)
        else:
            point.x -= 1
            if point.x < 0:
                points.remove(point)


def plot_points():

    unicorn.clear()
    for point in points:
        ##print(str(point.x) + "," + str( point.y) + ":" + str(point.colour[0]) + ":"+ str(point.colour[1]) + ":"+ str(point.colour[2]))
        unicorn.set_pixel(point.x, point.y, point.colour[0], point.colour[1], point.colour[2])
    unicorn.show()

unicorn_lastloop = -1
def unicorn_runloop(currentTime):
	global unicorn_lastloop
	if (currentTime < (unicorn_lastloop + 0.02)):
		return
	unicorn_lastloop = currentTime
	if len(points) < 10 and randint(0, 5) >1:
		points.append(LightPoint())
	plot_points()
	update_positions()

# single display frame
#  has compoenents
#   - Transition In Time
#   - Hold Time*
#   - Next Frame num* 
# (*optional)


class DisplayPattern():
	width = 8
	height = 8
#	lastDisplay = []
#	targetDisplay = []
#	lastDisplayTime = time.time()
#	targetDisplayTime = (time.time() + 100)

	curDisplay = [] #Current display pixel values

	frameStart = []  #piexl values at start of display
	frameTarget = [] #Final pixel values to display
	frameStartTransistionTime = time.time()
	frameEndTransistionTime = time.time()

	runningSequence = None
	logFn = None

	def __init__(self, logFn):
		self.logFn = logFn
		self.curDisplay = [[0 for x in range(self.width)] for y in range(self.height)] 
		self.frameStart = [[0 for x in range(self.width)] for y in range(self.height)] 
		self.frameTarget = [[0 for x in range(self.width)] for y in range(self.height)] 
		for x in range(0,self.width):
			for y in range(0,self.height):
				self.curDisplay[x][y] = Pixel(0,0,0)
				self.frameStart[x][y] = Pixel(0,0,0)
				self.frameTarget[x][y] = Pixel(0,0,0)
		self.setTargetDisplaySolidColour(255,255,255, float(10))

	def setTargetDisplaySolidColour(self, red, green, blue, transisitionTime):
		newTargetDisplay = [[0 for x in range(self.width)] for y in range(self.height)] 
		for x in range(0,self.width):
			for y in range(0,self.height):
				newTargetDisplay[x][y] = Pixel(red,green,blue)
		self.setTargetDisplay(newTargetDisplay, transisitionTime)

	def setTargetDisplay(self, newTargetDisplay, transisitionTime):
		#Save current frame as start
		#and set target frame to new value
		for x in range(0,self.width):
			for y in range(0,self.height):
				self.frameStart[x][y] = self.curDisplay[x][y].copy()
				self.frameTarget[x][y] = newTargetDisplay[x][y].copy()
		self.frameStartTransistionTime = time.time()
		self.frameEndTransistionTime = time.time() + transisitionTime

	def setSequence(self, newSequence):
		self.runningSequence = RunningSequence(newSequence, self.logFn)

	def notifyMotion(self):
		if self.runningSequence is not None:
			self.runningSequence.notifyMotion()

	def getNewCurValue(self, startVal, startTime, curTime, endVal, endTime):
		if (curTime>endTime):
			return endVal
		ratio = ((endVal - startVal)/(endTime-startTime))
		#print("startVal:%d endVal:%d RAT:%f" % (startVal, endVal, ratio))
		newVal = startVal + int(round(ratio * (curTime - startTime)))

		#we mighht have gone slightly past the desired value due to rounding
		# errors - check this and clip it back		
		if startVal<endVal:
			if newVal>endVal:
				newVal = endVal
		else:
			if newVal<endVal:
				newVal = endVal

		#Still getting errors when we quickly change direction
		# so final range check prevents these
		if newVal<0:
			newVal=0
		if newVal>255:
			newVal=255

		return newVal


	def runLoop(self, currentTime):
		if self.runningSequence is not None:
			self.runningSequence.runLoop(currentTime, self)

		#transform to values for currentTime
		for x in range(0,self.width):
			for y in range(0,self.height):
				curPixel = self.curDisplay[x][y]
				startPixel = self.frameStart[x][y]
				targetPixel = self.frameTarget[x][y]

				curPixel.r_val = self.getNewCurValue(
					startPixel.r_val, 
					self.frameStartTransistionTime, 
					currentTime,
					targetPixel.r_val,
					self.frameEndTransistionTime
				)
				curPixel.g_val = self.getNewCurValue(
					startPixel.g_val, 
					self.frameStartTransistionTime, 
					currentTime,
					targetPixel.g_val,
					self.frameEndTransistionTime
				)
				curPixel.b_val = self.getNewCurValue(
					startPixel.b_val, 
					self.frameStartTransistionTime, 
					currentTime,
					targetPixel.b_val,
					self.frameEndTransistionTime
				)


		for x in range(0,self.width):
			for y in range(0,self.height):
				curPixel = self.curDisplay[x][y]
				#if x==1:
				#	if y==1:
				#		print(
				#			("RED st:%03d " % self.frameStart[x][y].r_val) +
				#			("trg:%03d " % self.frameTarget[x][y].r_val) +
				#			("cur:%03d " % curPixel.r_val) +
				#			("TranTimeRemaining:%03d " % (self.frameEndTransistionTime - currentTime))
				#		) 
				if ((curPixel.r_val > 255) or (curPixel.r_val < 0)):
					print ("R val is " + str(curPixel.r_val))
				if ((curPixel.g_val > 255) or (curPixel.g_val < 0)):
					print ("G val is " + str(curPixel.g_val))
				if ((curPixel.b_val > 255) or (curPixel.b_val < 0)):
					print ("B val is " + str(curPixel.b_val))
				unicorn.set_pixel(x, y, curPixel.r_val, curPixel.g_val, curPixel.b_val)

		unicorn.show()

class UnicornControllerC(threading.Thread):
	global displayPattern, sequenceSaveFileName
	displayPattern = []
	running = True

	def __init__(self):
		threading.Thread.__init__(self)

	#Must be called after initalising object and befor run
	def setup(self,logFunction):
		global displayPattern
		displayPattern = DisplayPattern(logFunction)
		running = True

	def run(self):
		if os.path.isfile(sequenceSaveFileName):
			#print(displayPattern)
			f = open(sequenceSaveFileName,'r')
			jsonStr = f.read()
			f.close()
			displayPattern.setSequence(Sequence(json.loads(jsonStr)))
		self.running = True
		while self.running:
			currentTime = time.time()
			displayPattern.runLoop(currentTime)
			#update time is every hundredth of a second
			time.sleep(0.1)

	def notifyMotion(self):
		displayPattern.notifyMotion()

	def stopRunning(self):
		self.running = False

unicornController = UnicornControllerC()

