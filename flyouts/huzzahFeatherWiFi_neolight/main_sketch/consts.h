
#define NUM_LEDS    16 // Number of NeoPixels

#define NULL 0

#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL;
