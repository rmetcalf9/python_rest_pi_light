import uuid
from flask import Blueprint, jsonify, abort, request, make_response
import traceback
import datetime
import pytz


notifier_api_blueprint = Blueprint('notifier_api_blueprint',__name__);


targetURLJSONStrName = "targetPOSTURL";

@notifier_api_blueprint.route('/api/notifier/status')
def notifier_status():
  try:
    return notifier.status()
  except Exception as e:
    traceStr = traceback.format_exc()
    return make_response(jsonify(
      response="error",
      message=str(e),
      trace=traceStr
    ),400)

@notifier_api_blueprint.route('/api/notifier', methods=['POST'])
def notifier_addnotification():
  if not request.json:
    abort(415)
  jsonObj = request.json
  if not notifier.verifydSecret(jsonObj):
    abort(403)

  try:
    return notifier.addNewNotification(jsonObj)
  except Exception as e:
    traceStr = traceback.format_exc()
    return make_response(jsonify(
      response="error",
      message=str(e),
      trace=traceStr
    ),400)

@notifier_api_blueprint.route('/api/notifier/<externalID>', methods=['DELETE'])
def notifier_delnotification(externalID):
  if not request.json:
    abort(415)
  jsonObj = request.json
  if not notifier.verifydSecret(jsonObj):
    abort(403)
  abort(406)

class NotificationRecordC:
  uniqueID='TBC'
  notificationName='TBC'
  targetURL='TBC'
  externalSystemID='TBC'
  externalID='TBC'
  creationTime=None
  lastRegisterTime=None

  def __init__(self, notificationName, targetURL, externalID, externalSystemID):
    curTime = datetime.datetime.now(pytz.timezone("UTC"))
    self.uniqueID = str(uuid.uuid4())
    self.notificationName = notificationName
    self.targetURL = targetURL
    self.externalSystemID = externalSystemID
    self.externalID = externalID
    self.creationTime = curTime
    self.lastRegisterTime = curTime

  def _caculatedDict(self):
    return self.__dict__
    
  #called when reregistering existing notification
  def reregister(self, notificationName, targetURL):
    curTime = datetime.datetime.now(pytz.timezone("UTC"))

    self.notificationName = notificationName
    self.targetURL = targetURL
    self.lastRegisterTime = curTime
    

class NotifierC:
  secrets=None
  notificationTargets={}
  expiryDuration=datetime.timedelta(hours=2)
  #expiryDuration=datetime.timedelta(minutes=1)
  
  maxFrequencyToCheckForExpiry=datetime.timedelta(seconds=120)
  #maxFrequencyToCheckForExpiry=datetime.timedelta(seconds=5)
  
  lastTimeCheckedForExpired=datetime.datetime.now(pytz.timezone("UTC"))

  def setSecrets(self, secrets):
    self.secrets = secrets

  def verifydSecret(self, requestJSONObj):
    if self.secrets is None:
      return False
    if requestJSONObj is None:
      return False
    if 'secret' not in requestJSONObj:
      return False
    if requestJSONObj['secret'] is None:
      return False
    return (requestJSONObj['secret'] == self.secrets['notifierSecret'])

  def removeExpiredNotificationTargets(self):
    curTime=datetime.datetime.now(pytz.timezone("UTC"))
    if curTime < (self.lastTimeCheckedForExpired + self.maxFrequencyToCheckForExpiry):
      return
    self.lastTimeCheckedForExpired=datetime.datetime.now(pytz.timezone("UTC"))
    newNotificationTargets={}
    for k, v in self.notificationTargets.items():
      if curTime < (v.lastRegisterTime + self.expiryDuration):
        newNotificationTargets[k]=v
    if len(self.notificationTargets.items()) == len(newNotificationTargets.items()):
      # no changes no need to keep new array
      return
    self.notificationTargets = newNotificationTargets

  def status(self):
    self.removeExpiredNotificationTargets()
    return jsonify(
      notificationTargets=list(map(lambda kv: kv[1]._caculatedDict(), self.notificationTargets.items()))
    )

  def ensureFieldPresent(self, jsonObj, identifier):
    if identifier not in jsonObj:
      raise Exception(identifier + " required")
    

  def addNewNotification(self, jsonObj):
    self.ensureFieldPresent(jsonObj,"externalSystemID")
    self.ensureFieldPresent(jsonObj,"externalID")
    self.ensureFieldPresent(jsonObj,"notificationName")
    self.ensureFieldPresent(jsonObj,targetURLJSONStrName)
    
    notificationTargetKey = jsonObj["externalSystemID"] + ":_£$%" + jsonObj["externalID"]
    
    if notificationTargetKey in self.notificationTargets:
      self.notificationTargets[notificationTargetKey].reregister(
        jsonObj["notificationName"], 
        jsonObj[targetURLJSONStrName]
      )
      return jsonify(
        result='ok',
        action='updated'
      )
    else:
      self.notificationTargets[notificationTargetKey] = NotificationRecordC(
        jsonObj["notificationName"], 
        jsonObj[targetURLJSONStrName], 
        jsonObj["externalID"], 
        jsonObj["externalSystemID"]
      )
    return jsonify(
      result='ok',
      action='created'
    )


notifier = NotifierC()

