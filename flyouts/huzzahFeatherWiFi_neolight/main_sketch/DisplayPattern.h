#include "consts.h"
#include <Adafruit_NeoPixel.h>

class Pixel {
  private:
    unsigned int pixel_index = 0;
    unsigned int r = 0;
    unsigned int b = 0;
    unsigned int g = 0;

  public:
    Pixel(unsigned int ppixel_index, unsigned int pr, unsigned int pg, unsigned int pb) {
      pixel_index = ppixel_index;
      r = pr;
      g = pg;
      b = pb;
    };
    void display(Adafruit_NeoPixel* leds) {
      leds->setPixelColor(pixel_index,r,g,b); 
    };
    unsigned int getRed() {return r;};
    unsigned int getGreen() {return g;};
    unsigned int getBlue() {return b;};
    void setRed(unsigned int val) {r = val;};
    void setGreen(unsigned int val) {g = val;};
    void setBlue(unsigned int val) {b = val;};
    void setColourMatch(Pixel* othPixel) {
      r = othPixel->getRed();
      g = othPixel->getGreen();
      b = othPixel->getBlue();
    }
};

class DisplayPattern {
  public:
    DisplayPattern(unsigned long currentTime);
    ~DisplayPattern();

    void runLoop(unsigned long currentTime, Adafruit_NeoPixel* leds);
    void setTargetDisplaySolidColour(unsigned long currentTime, unsigned int red, unsigned int green, unsigned int blue, unsigned long transisitionTime);
    void setTargetDisplay(unsigned long currentTime, Pixel* newTargetDisplay[], unsigned long transisitionTime);
    unsigned int getNewCurValue(unsigned int startVal, unsigned long startTime, unsigned long curTime, unsigned int endVal, unsigned long endTime);
    
  private:
    Pixel* curDisplay[NUM_LEDS]; //Current display pixel values
    Pixel* frameStart[NUM_LEDS]; //piexl values at start of display
    Pixel* frameTarget[NUM_LEDS]; //Final pixel values to display

    unsigned long frameStartTransistionTime = 0;
    unsigned long frameEndTransistionTime = 0;

};
