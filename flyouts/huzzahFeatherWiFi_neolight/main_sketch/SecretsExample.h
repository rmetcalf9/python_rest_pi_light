// Example Secret file
// Copy to Secret.h in the same location and add your own wifi details
//  and certificate
// this file is in GitIgnore and will not go into source control

#define SECRET_WIFISSID "XXX"
#define SECRET_WIFIPASS "YYY"

static const uint8_t secret_x509[] PROGMEM = {
  0x00, 0x00, ...
};

static const uint8_t secret_rsakey[] PROGMEM = {
  0x00, 0x00, ...
};

//Light server details
const char* light_server_host     = "light.abc.com";
const int light_server_port       = 9100;
const char* light_server_notifier_url = "/api/notifier";

// If needed, update IFTTT SHA1 Fingerprint used for cert verification:
// $ openssl s_client -servername maker.ifttt.com -connect light.metcarob-home.com:8000 | openssl x509 -fingerprint -noout
// (Replace colons with spaces in result)

const char* light_server_SHA1Fingerprint="1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A 1A";  // See SHA1 comment above 

#define SECRET_LIGHTSERVER_SECRET "aabbcc"

