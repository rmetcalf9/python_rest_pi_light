# Neolight Flyout for Python Rest Pi Light

This directory contains the program to run on the flyout. This can be loaded into the Audrino IDE and used to program the Hazzah feather ESO8266.

## Secrets

Copy SecretsExample.h to Secrets.h and alter with your own values.


## Example service call

'''
{
"transistionTime":"30",
"pixelValues": ["(255,244, 255)"],
"brightness": 255
}
'''
transistionTime is in miliseconds (10000 = 10 seconds)
brightness is between 0 and 255


