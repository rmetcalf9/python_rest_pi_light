#include <Adafruit_NeoPixel.h>
#include "DisplayPattern.h"


class RJM_NeoPixel {
  private:
    Adafruit_NeoPixel* leds = NULL;
    DisplayPattern* displayPattern = NULL;
    Pixel* GeneratePixel(unsigned int ppixel_index, const char * buf);
  public:
    RJM_NeoPixel();
    ~RJM_NeoPixel();
    void Setup();
    void Render(unsigned long currentTime);
    void SetFrame(unsigned long currentTime, String incommingJSON, unsigned int *returnVal, char* resultJSON);
};
