/*
 * Main Sketch for my device that drives some neoPixels based on notifications from my light server
 * Thrown together from various examples
*/


// notes on delay code: https://www.forward.com.au/pfod/ArduinoProgramming/TimingDelaysInArduino.html
// unsigned long REFRESH_NOTIFICATION_DELAY_TIME = 1000 * 10; // 10 sec
unsigned long REFRESH_NOTIFICATION_DELAY_TIME = 1000 * 60 * 60 * 2; // 2 hours
unsigned long refreshNotificationDelayStart = 0; // the time the delay started

unsigned long NEOPIXELRENDER_NOTIFICATION_DELAY_TIME = 200; // 0.2 sec
unsigned long neopixelDelayStart = 0; // the time the delay started


const int led = 2; //For Huzzah LEDs are 0 and 2
// 1 is the red light near the usb connector
// 2 is the blue light near the wifi areial

#define LED_OFF_STATE 1
#define LED_ON_STATE 0

#define FRAME_ENDPOINT "/frame"

/*
  HelloServerSecure - Simple HTTPS server example

  This example demonstrates a basic ESP8266WebServerSecure HTTPS server
  that can serve "/" and "/inline" and generate detailed 404 (not found)
  HTTP respoinses.  Be sure to update the SSID and PASSWORD before running
  to allow connection to your WiFi network.

  IMPORTANT NOTES ABOUT SSL CERTIFICATES

  1. USE/GENERATE YOUR OWN CERTIFICATES
    While a sample, self-signed certificate is included in this example,
    it is ABSOLUTELY VITAL that you use your own SSL certificate in any
    real-world deployment.  Anyone with the certificate and key may be
    able to decrypt your traffic, so your own keys should be kept in a
    safe manner, not accessible on any public network.

  2. HOW TO GENERATE YOUR OWN CERTIFICATE/KEY PAIR
    A sample script, "make-self-signed-cert.sh" is provided in the
    ESP8266WiFi/examples/WiFiHTTPSServer directory.  This script can be
    modified (replace "your-name-here" with your Organization name).  Note
    that this will be a *self-signed certificate* and will *NOT* be accepted
    by default by most modern browsers.  They'll display something like,
    "This certificate is from an untrusted source," or "Your connection is
    not secure," or "Your connection is not private," and the user will
    have to manully allow the browser to continue by using the
    "Advanced/Add Exception" (FireFox) or "Advanced/Proceed" (Chrome) link.

    You may also, of course, use a commercial, trusted SSL provider to
    generate your certificate.  When requesting the certificate, you'll
    need to specify that it use SHA256 and 1024 or 512 bits in order to
    function with the axTLS implementation in the ESP8266.

  Interactive usage:
    Go to https://esp8266-webupdate.local/firmware, enter the username
    and password, and the select a new BIN to upload.

  To upload through terminal you can use:
  curl -u admin:admin -F "image=@firmware.bin" esp8266-webupdate.local/firmware

  Adapted by Earle F. Philhower, III, from the HelloServer.ino example.
  This example is released into the public domain.
*/
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServerSecure.h>
#include <ESP8266mDNS.h>
#include "Secrets.h"
#include "HTTPSRedirect.h"
#include "neoPixel.h"

RJM_NeoPixel neoPixel;

const char* ssid = SECRET_WIFISSID;
const char* password = SECRET_WIFIPASS;

ESP8266WebServerSecure server(443);

// The certificate is stored in PMEM = secret_x509

// And so is the key.  These could also be in DRAM = secret_rsakey

void handleRoot() {
  server.send(200, "text/plain", "Hello from esp8266 over HTTPS!");
}

void handlePostFrame() {
  if (server.hasArg("plain")== false){ //Check if body received
      server.send(415, "text/json", "{\"Result\":\"ERROR\", \"Message\": \"No body posted\"}");
      return;
  }
  unsigned int returnVal = -1;
  char retJSONBUF[255] = "";

  neoPixel.SetFrame(millis(), server.arg("plain"), &returnVal, retJSONBUF);
  server.send(returnVal, "text/json", retJSONBUF);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}


void setup(void) {
  neoPixel.Setup();
  
  pinMode(led, OUTPUT);
  digitalWrite(led, LED_OFF_STATE);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.setServerKeyAndCert_P(secret_rsakey, sizeof(secret_rsakey), secret_x509, sizeof(secret_x509));

  server.on("/", handleRoot);
  server.on(FRAME_ENDPOINT, handlePostFrame);

  //server.on("/inline", []() {
  //  server.send(200, "text/plain", "this works as well");
  //});

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTPS server started");

  //Setup Delays
  refreshNotificationDelayStart = millis();
  neopixelDelayStart = millis();

  //don't wait 2 hours before first update
  tellLightServerThatINeedToRecieveNotifications();
}

//boolean ledon = false;
void tellLightServerThatINeedToRecieveNotifications(void) {
  Serial.println("Call tellLightServerThatINeedToRecieveNotifications");
  //Test code to flash the LED
  //if (ledon) {
  //  digitalWrite(led, 0);
  //  ledon = false;
  //} else {
  //  digitalWrite(led, 1);
  //  ledon = true;
  //}

  //https://light.metcarob-home.com:8000/api/notifier
  //{
  //"secret":"XXX", 
  //"externalSystemID": "HUZZAHFlyout001",
  //"externalID": "001",
  //"notificationName": "lightChange",
  //"targetGETURL": "https://192.168.1.177/lightchange"
  //} 

  HTTPSRedirect client(light_server_port);
  if (!client.connect(light_server_host, light_server_port)) {
    Serial.println("connection failed");
    return;
  }
  Serial.println("Connection established");

  if (!client.verify(light_server_SHA1Fingerprint, light_server_host)) {
      Serial.println("certificate doesn't match. will not send message.");
      return;
  } 

  String postData = "{";
  postData += "\"secret\":\""; postData += SECRET_LIGHTSERVER_SECRET; postData += "\",";
  postData += "\"externalSystemID\": \"HUZZAHFlyout001\",";
  postData += "\"externalID\": \"001\",";
  postData += "\"notificationName\": \"lightChange\",";
  postData += "\"targetPOSTURL\": \"https://"; postData += WiFi.localIP().toString(); postData += FRAME_ENDPOINT; postData += "\"";
  postData += "}";

  client.print("POST "); client.print(light_server_notifier_url); client.println(" HTTP/1.1");
  client.print("Host: "); client.println(light_server_host);
  client.println("Cache-Control: no-cache");
  client.println("Content-Type: application/json");
  client.print("Content-Length: "); client.println(postData.length());
  client.println();
  client.println(postData);

  Serial.println("Read response");

/*
HTTP/1.0 415 UNSUPPORTED MEDIA TYPE
Content-Type: text/html
Content-Length: 208
Access-Control-Allow-Origin: http://localhost:8080
Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization
Vary: Cookie
Set-Cookie: session=eyJfcGVybWFuZW50Ijp0cnVlfQ.Dshc9Q.dcA5TGWhz6twSAqFUFCEVv0v7uE; Expires=Sat, 10-Nov-2018 12:18:41 GMT; HttpOnly; Path=/
Server: Werkzeug/0.14.1 Python/3.5.3
Date: Sat, 10 Nov 2018 12:13:41 GMT

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>415 Unsupported Media Type</title>
<h1>Unsupported Media Type</h1>
<p>The server does not support the media type transmitted in the request.</p>
 */

  while (client.connected())
  {
    if ( client.available() )
    {
      char str=client.read();
     Serial.print(str);
    }      
  }
  Serial.println("");
  Serial.println("End of response");

  client.flush();
  client.stop();

  return;
}


void loop(void) {
  unsigned long currentTime = millis();
  server.handleClient();

  if (((currentTime - neopixelDelayStart) >= NEOPIXELRENDER_NOTIFICATION_DELAY_TIME)) {
    neopixelDelayStart += NEOPIXELRENDER_NOTIFICATION_DELAY_TIME; // this prevents drift in the delays
    neoPixel.Render(currentTime);
  }

  if (((currentTime - refreshNotificationDelayStart) >= REFRESH_NOTIFICATION_DELAY_TIME)) {
    refreshNotificationDelayStart += REFRESH_NOTIFICATION_DELAY_TIME; // this prevents drift in the delays
    tellLightServerThatINeedToRecieveNotifications();
  }
}
