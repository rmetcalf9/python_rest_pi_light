if [ $# -ne 1 ]; then
  echo "Must supply one argument"
  exit 1
fi 
if [  ! -e ${1} ]; then
  echo "${1} dosen't exist"
  exit 1
fi

echo "Setting sequence to ${1}"
curl -k -H "Content-Type: application/json" -d@${1} https://light.metcarob-home.com:8000/api/unicorn/setsequence
exit $?
