from userdatabase import User
from flask import Blueprint, abort, g, jsonify, request, json
from auth import requires_auth, requires_ADMIN_role, requires_role
from functools import partial
from app import mainApp
from service_code.sensor import sensor_api_blueprint, sensorReader
from service_code.unicornh import unicorn_api_blueprint, unicornController
from service_code.notifier import notifier_api_blueprint, notifier

api_blueprint = Blueprint('api_blueprint',__name__);

secretsSaveFileName = './py_packages/service_code/secrets.json'


def api_blueprint_registration(logFunction):
	def runLightLogFunction(message):
		logFunction("run_light," + message)

	f = open(secretsSaveFileName, 'r')
	secretJsonStr = f.read()
	f.close()
	secrets = json.loads(secretJsonStr)
	notifier.setSecrets(secrets)

	mainApp.app.register_blueprint(api_blueprint)
	mainApp.app.register_blueprint(sensor_api_blueprint)
	mainApp.app.register_blueprint(unicorn_api_blueprint)
	mainApp.app.register_blueprint(notifier_api_blueprint)
	sensorReader.setUnicornController(unicornController)
	sensorReader.start()
	unicornController.setup(runLightLogFunction)
	unicornController.start()
	mainApp.registerShutdownApp(moduleShutdownFunction)

def moduleShutdownFunction():
	sensorReader.stopRunning()
	unicornController.stopRunning()
	sensorReader.join()
	unicornController.join()
#	print("moduleShutdownFunction")

#Example path that is public
@api_blueprint.route('/api')
def api():
	return "Blueprint API page other"

#Example path that requires just a login
@api_blueprint.route('/api/loginreq')
@requires_auth
def api_loginreq():
	return "Blueprint API page login " + g.user.id


#Example path that requires ADMIN role
@api_blueprint.route('/api/login/ADMIN')
@requires_ADMIN_role
def api_adminloginreq():
	return "Blueprint API page ADMIN " + g.user.id

#define a decoration for a custom role
#NOTE must call masterAuthObj.registerInternalRoleName in __INIT__ for every internal role name
requires_CUSTOM_role = partial(requires_role, internalRoleName='CUSTOM')

#Example path that requires ADMIN role
@api_blueprint.route('/api/login/CUSTOM')
@requires_CUSTOM_role
def api_customloginreq():
	return "Blueprint API page - CUSTOM " + g.user.id
